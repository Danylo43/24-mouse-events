## 1. Що таке події в JavaScript і для чого вони використовуються?

Події використовуються для того, щоб виконувати певні дії у відповідь на взаємодію користувача з веб-сторінкою

## 2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.

- click
 ```
 div.addEventListener('click', function() {
    alert('Натиснуто!');
  });
```


- dblclick
```
 div.addEventListener('dblclick', function() {
    div.style.backgroundColor = 'lightgreen';
    message.textContent = 'Подію dblclick виявлено!';
  });
```

- mousemove

 ```
 div.addEventListener('mousemove', function(event) {
    console.log(`Координати миші: X=${event.clientX}, Y=${event.clientY}`);
  });
```


- mouseover
```
 div.addEventListener('mouseover', function() {
    div.style.backgroundColor = 'lightgreen';
  });
```


- mouseup 
 ```
div.addEventListener('mouseup', function() {
    div.style.backgroundColor = 'orange';
    message.textContent = 'Подію mouseup виявлено!';
  });
```


- mouseleave
 ```
 div.addEventListener('mouseleave', function() {
    div.style.backgroundColor = 'lightblue';
    message.textContent = 'Подію mouseleave виявлено!';
  });
```


- mousedown
```
  div.addEventListener('mousedown', function() {
    div.style.backgroundColor = 'yellow';
    message.textContent = 'Подію mousedown виявлено!';
  });
```


## 3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

Подія contextmenu спрацьовує, коли користувач викликає контекстне меню на елементі, зазвичай при натисканні правої кнопки миші. Це дозволяє розробникам створювати власні контекстні меню замість стандартного меню браузера.
