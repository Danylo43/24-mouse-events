  "use strict";
//1. Додати новий абзац по кліку на кнопку:
//По кліку на кнопку <button id="btn-click">Click Me</button>, 
//створіть новий елемент <p> з текстом "New Paragraph" і додайте 
//його до розділу <section id="content">
  document.getElementById("btn-click").addEventListener("click", () => {
    const newParagraph = document.createElement("p");
    newParagraph.textContent = "New Paragraph";
    document.getElementById("content").appendChild(newParagraph);
  });

  //2. Додати новий елемент форми із атрибутами:
//Створіть кнопку з id "btn-input-create", додайте 
//її на сторінку в section перед footer. По кліку 
//на створену кнопку, створіть новий елемент <input> 
//і додайте до нього власні атрибути, наприклад, 
//type, placeholder, і name. та додайте його під кнопкою.
  const footer = document.querySelector("footer");
  const createInputButton = document.createElement("button");
  createInputButton.id = "btn-input-create";
  createInputButton.textContent = "Створити новий input";
  
  const container = document.querySelector(".main__container");
  container.insertBefore(createInputButton, footer);

  createInputButton.addEventListener("click", () => {
    const newInput = document.createElement("input");
    newInput.type = "text";
    newInput.placeholder = "Введіть текст";
    newInput.name = "custom-input";
    
    createInputButton.insertAdjacentElement("afterend", newInput);
  });
